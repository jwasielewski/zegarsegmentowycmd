#include <vector>
#include <windows.h>

#include "Segment.h"
#include "Config.h"

#pragma once
class Zegar
{
public:
	Zegar(void);
	~Zegar(void);

	void run();
private:
	void repaint();
	void updateTime();
	void curPos(int, int);

	HANDLE hConsole;

	Config::Konfiguracja *k;
	std::vector<Segment> segmenty;
};

