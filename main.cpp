#include "Zegar.h"

#include <iostream>
#include <windows.h>

BOOL WINAPI ConsoleHandler(DWORD CEvent)
{
	switch(CEvent)
    {
    case CTRL_C_EVENT:
		std::cout << std::endl;
        SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);
		std::system("cls");
		exit(0);
        break;
    }
    return TRUE;
}

int main()
{
	std::system("cls");
	if (!SetConsoleCtrlHandler( (PHANDLER_ROUTINE)ConsoleHandler,TRUE))
	{
		std::cout << "[!UWAGA!] Program nie bedzie reagowal poprawnie na sygnal Ctrl-C" <<std::endl;
	}
	(new Zegar())->run();
}