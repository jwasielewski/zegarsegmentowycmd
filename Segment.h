#include <string>
#include <vector>

#pragma once
class Segment
{
public:
	Segment(void);
	~Segment(void);

	void setWidth(int);
	int getWidth();
	void setNumber(int);
	
	std::string getLine(int);

	static void setChar(char, char, char);
	static void initVector();

private:
	int width;
	int number;
	static std::vector<std::string> numbers;
};

