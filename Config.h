#include <string>

#pragma once
class Config
{
public:
	~Config();

	static Config& getInstance()
	{
		static Config instance;
		return instance;
	}

	class Konfiguracja
	{
	public:
		char bgChar;
		char fgChar;
		char fgUnChar;
		unsigned int bgColor;
		unsigned int fgColor;
		unsigned int fgUnColor;
	};

	Konfiguracja* getConfig();

private:
	Config();

	static const std::string fileName;
};

