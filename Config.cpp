#include "Config.h"
#include <fstream>
#include <ios>
#include <string>

const std::string Config::fileName = std::string("konfiguracja.conf");

Config::Config(void)
{
}

Config::~Config(void)
{
}

Config::Konfiguracja* Config::getConfig()
{
	Config::Konfiguracja *k = new Config::Konfiguracja;

	std::ifstream file(fileName.c_str());
	if(file.is_open())
	{
		file >> k->bgChar;
		file >> k->fgChar;
		file >> k->fgUnChar;
		file >> std::hex >> k->bgColor;
		file >> std::hex >> k->fgColor;
		file >> std::hex >> k->fgUnColor;
	}
	else
		return NULL;

	file.close();
	return k;
}
