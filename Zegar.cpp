#include "Zegar.h"
#include "Segment.h"
#include "Config.h"

#include <iostream>
#include <ctime>
#include <string>
#include <cstdlib>

#include <windows.h>

Zegar::Zegar(void)
{
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	Segment::initVector();
	segmenty.resize(8);
	for(auto &x : segmenty)
	{
		x.setWidth(13);
	}
	segmenty[2].setWidth(1);
	segmenty[2].setNumber(10);
	segmenty[5].setWidth(1);
	segmenty[5].setNumber(10);

	k = Config::getInstance().getConfig();
	Segment::setChar(k->bgChar, k->fgChar, k->fgUnChar);
}


Zegar::~Zegar(void)
{
}

void Zegar::run()
{
	while(true)
	{
		updateTime();
		repaint();
		for(time_t t = time(0) + 1; time(0) < t; );
	}
}

void Zegar::updateTime()
{
	time_t t = time(0);
	struct tm tim;
	localtime_s(&tim, &t);

	segmenty[0].setNumber(tim.tm_hour / 10);
	segmenty[1].setNumber(tim.tm_hour % 10);

	segmenty[3].setNumber(tim.tm_min / 10);
	segmenty[4].setNumber(tim.tm_min % 10);

	segmenty[6].setNumber(tim.tm_sec / 10);
	segmenty[7].setNumber(tim.tm_sec % 10);
}

void Zegar::repaint()
{
	int cur_width = 0;
	for(unsigned int j=0; j<segmenty.size(); ++j)
	{
		for(int i=0; i<11; ++i)
		{
			curPos(cur_width, i);
			std::string tmp = segmenty[j].getLine(i);
			for(auto &z : tmp)
			{
				if(z == k->bgChar)
				{
					SetConsoleTextAttribute(hConsole, k->bgColor);
					std::cout << z;
				}
				else if(z == k->fgChar)
				{
					SetConsoleTextAttribute(hConsole, k->fgColor);
					std::cout << z;
				}
				else
				{
					SetConsoleTextAttribute(hConsole, k->fgUnColor);
					std::cout << z;
				}
			}
		}
		cur_width += segmenty[j].getWidth();
	}
	SetConsoleTextAttribute(hConsole, 7);
}

void Zegar::curPos(int x, int y)
{
  CONSOLE_SCREEN_BUFFER_INFO csbiInfo;
  GetConsoleScreenBufferInfo(hConsole, &csbiInfo);
  csbiInfo.dwCursorPosition.X=x;
  csbiInfo.dwCursorPosition.Y=y;
  SetConsoleCursorPosition(hConsole, csbiInfo.dwCursorPosition);
}
